export const varStorageGlobal = {
    VERSION_APP: '0.0.10',
    TOKEN: 'TOKEN-AUTHORIZE',
    USER_ONLINE: 'USER-ONLINE',
    PROFILE_COMPANY: 'PROFILE-COMPANY',
    URL_API: 'URL-API',
};

export interface IUserLogin {
    User: string;
    Password: string;
    Remenber: boolean;
}

export interface IUser {
    user: string;
    password: string;
    remember: boolean;
}

export interface IResponseLoginUser {
    token: string;
    expiration: '2020-01-13T22:51:13.4678588-04:00';
    statusUser: 1;
}

export interface IGetToken {
    expiration: string;
    statusUser: number;
    token: string;
}

export interface IUserOnline {
    id: number;
    nickName: string;
    name: string;
    lastName: string;
    idCompany: string;
    status: number;
    lastLogin: string;
    idRol: number;
    urlImg: string;
    note: string;
    idSecret: string;
}

export interface IProfileCompany {
    idCompany: string;
    longName: string;
    shortName: string;
    slogan: string;
    phone: string;
    address: string;
    rnc: string;
    status: number;
    versionAppMovil: string;
    versionAppDesktop: string;
    urlDownloadApp: string;
    urlCloudDownloadApp: string;
}
