import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'select-Gender',
    templateUrl: './gender.component.html',
    styleUrls: ['./gender.component.scss'],
})
export class GenderComponent implements OnInit {
    genders = ['Male', 'Female', 'Other'];

    gender: [{ type: 'required'; message: 'Please select your gender' }];
    constructor() {}

    ngOnInit() {}
}
