import { Component, Input, forwardRef, OnInit } from '@angular/core';

import { FormControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material';
import { Subject } from 'rxjs';
@Component({
    selector: 'app-input-demo',
    templateUrl: './input-demo.component.html',
    styleUrls: ['./input-demo.component.scss'],

    providers: [
        {
            provide: MatFormFieldControl,
            useExisting: forwardRef(() => InputDemoComponent),
        },
    ],
})
export class InputDemoComponent extends MatFormFieldControl<
    InputDemoComponent
> {
    stateChanges: Subject<void> = new Subject<void>();
    id = '0';

    placeholder = 'My Custom Input';
    ngControl = null;
    focused = false;
    empty = true;
    value: any;

    // constructor() {

    //     this.formControl.valueChanges.subscribe(value => {
    //         this.value = value;
    //         this.stateChanges.next();
    //     });
    // }
    get shouldPlaceholderFloat() {
        return !!this.value;
    }

    required = false;

    errorState = false;

    @Input()
    set disabled(disabled) {
        this._disabled = disabled;
        disabled ? this.formControl.disable() : this.formControl.enable();
        this.stateChanges.next();
    }
    get disabled() {
        return this._disabled;
    }
    private _disabled = false;

    formControl = new FormControl();

    options = ['One', 'Two', 'Three'];

    setDescribedByIds(ids: string[]): void {}

    onContainerClick(): void {}
}
