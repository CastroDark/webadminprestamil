import {
    Component,
    OnInit,
    Output,
    EventEmitter,
    Input,
    ViewChild,
} from '@angular/core';
import {
    NgbModal,
    ModalDismissReasons,
    NgbModalOptions,
} from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subscription } from 'rxjs';
import { DemoValidationComponent } from '../modals/demovalidate/demo-validation/demo-validation.component';
import { LoadingService } from '../../core/services/loadingSpinner/loading.service';
@Component({
    selector: 'displayModals',
    templateUrl: './displayModals.component.html',
    styleUrls: ['./displayModals.component.scss'],
})
export class DisplayModalsComponent implements OnInit {
    // @Input() public user;
    // @Output() passEntry: EventEmitter<any> = new EventEmitter();

    @Input() name: any;
    @Input() data: any;
    @Output() close: EventEmitter<any> = new EventEmitter();
    // @Input() open2: EventEmitter<any> = new EventEmitter();
    @Input() events: Observable<void>;
    closeResult: string;
    modalOptions: NgbModalOptions;
    private eventsSubscription: Subscription;

    constructor(
        private modalService: NgbModal,
        private loading: LoadingService,
    ) {
        this.modalOptions = {
            backdropClass: 'customBackdrop',
            size: 'xl',
            windowClass: 'xlModal',
        };
    }

    ngOnInit() {
        //console.log(this.user);
        this.eventsSubscription = this.events.subscribe(data => {
            this.open(this.name);
        });
        //     //console.log(data);
        //     this.open(this.childProperty);
        // });
        // setTimeout(() => {
        //     //console.log(this.name);
        //     this.open(this.name);
        //     // if (this.childProperty) {
        //     //     this.open(this.childProperty);
        //     //     //     //   this.open(this.childProperty);
        //     //     //     this.open(this.childProperty);
        //     //     //     console.log(this.childProperty);
        //     // }
        // }, 100);
    }

    // passBack() {
    //     //this.passEntry.emit(this.user);
    //     // this.activeModal.close(this.user);
    //     // this.activeModal.close(this.user);
    // }

    ngOnDestroy() {
        //this.eventsSubscription.unsubscribe();
    }

    open(content) {
        this.loading.show();

        setTimeout(() => {
            const modalRef = this.modalService.open(
                DemoValidationComponent,
                this.modalOptions,
            );
            modalRef.componentInstance.data = this.data;
            modalRef.componentInstance.passEntry.subscribe(receivedEntry => {
                this.closeModal(receivedEntry);
            });
        }, 500);
        this.loading.hide();

        // modalRef.componentInstance.passEntry.subscribe(receivedEntry => {
        //     console.log(receivedEntry);
        // });
        // this.modalService.open(content, this.modalOptions).result.then(
        //     result => {
        //         console.log('1' + result);
        //         this.closeResult = `Closed with: ${result}`;
        //     },
        //     reason => {
        //         console.log(' 2 ' + this.closeResult);
        //         this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        //     },
        // );
    }

    private getDismissReason(reason: any): string {
        //this.close.emit(reason);
        if (reason === ModalDismissReasons.ESC) {
            console.log('1');
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            console.log('2');
            return 'by clicking on a backdrop';
        } else {
            console.log('3');
            return `with: ${reason}`;
        }
    }

    closeModal(receivet) {
        //   console.log(receivet);
        this.close.emit(receivet);
    }
}
