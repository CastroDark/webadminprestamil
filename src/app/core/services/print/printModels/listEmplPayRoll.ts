import UtilsPrint from "../utilsPrint";
const functUtils = new UtilsPrint();
import * as jsPDF from "jspdf";
import { resolve } from "url";

class ListEmpPayRoll {
  linesUsed: number = 0;
  maxHeigthNewPage: number = 270;
  initLineDataPage: number = 80;
  numberPage = 1;
  xInitPosition = 8;

  header(doc) {
    //doc = jsService.titleReport(doc);
    //HEADER
    doc = functUtils.headerTitleCompanyLeft(doc);
    doc = functUtils.dateRight(doc);
    doc = functUtils.titleRptCenter(doc, "-- Reporte Nomina de pago --");
    doc = functUtils.lineHorizontal(doc, 38);

    return doc;
  }

  print(doc: jsPDF): Promise<any> {
    doc = this.getList1(doc);
    return doc;
  }

  getList1(doc) {
    doc.page = 1;
    doc = this.header(doc);

    this.linesUsed = this.initLineDataPage;

    // Before adding new content
    doc = functUtils.footerNumberPage(doc, String(this.numberPage));
    let title = ["Viernes 23, sep 2020   SEMANA # 3-20"];
    doc = functUtils.titleDetailsParams(doc, title);

    let count = 1;
    while (true) {
      // Unreachable code
      if (count == 100) {
        break;
      }

      // if (linesUsed >= maxHeigthNewPage) {
      //   doc = this.newPage(doc, 20);
      //   linesUsed = initLinePage;
      // }

      //#region Print Text
      this.checkHeigth(doc).then(data => {
        doc = data;
      });
      this.textoBold(
        doc,
        10,
        this.xInitPosition,
        this.linesUsed,
        `[ ${count} ] `
      );
      this.textoBold(
        doc,
        10,
        this.xInitPosition + 100,
        this.linesUsed + 5,
        `[ ${count} ] `
      );
      //Generate BarCode
      let imgCode = functUtils.generateBarCode("1254442155" + count);
      doc.addImage(imgCode.src, "JPEG", 150, (this.linesUsed += 5), 30, 15);

      // doc = this.textoBold(
      //   doc,
      //   11,
      //   xInitPosition + 20,
      //   line,
      //   String(" - apodo -").toUpperCase()
      // );

      // //name employee
      // doc = this.textoNormal(
      //   doc,
      //   10,
      //   xInitPosition + 10,
      //   (line += 6),
      //   "NOMBRE DEL EMPLEADO LARGO PARA DEMO  " + count
      // );

      // //Amount
      // doc = this.textoMonto(
      //   doc,
      //   10,
      //   xInitPosition + 50,
      //   (line += 4),
      //   `Monto A Cobrar : $1,500.00`
      // );

      // //line firma
      // doc = this.textoNormal(
      //   doc,
      //   7,
      //   xInitPosition + 120,
      //   (line += 1),
      //   `_________________________________________________`
      // );

      // //Amount
      // doc = this.textoNormal(
      //   doc,
      //   7,
      //   xInitPosition + 140,
      //   (line += 3),
      //   `Firma, Recibio Conforme`
      // );
      //#endregion

      this.linesUsed += 15;
      count++;
    }

    return doc;
  }

  checkHeigth(doc): Promise<any> {
    const promise = new Promise<any>((resolve, reject) => {
      //check to New Page
      if (this.linesUsed >= this.maxHeigthNewPage) {
        doc = this.newPage(doc, 20);
        this.linesUsed = this.initLineDataPage;
        resolve(doc);
      } else {
        resolve(doc);
      }
    });

    return promise;
  }

  newPage(doc, height: number) {
    doc.addPage();
    doc = this.header(doc);
    //add titleDetaisParams
    let title = ["Nomina de Pago : Viernes 23, sep 2020", "Semana : 44"];
    doc = functUtils.titleDetailsParams(doc, title);

    //footer number page
    doc = functUtils.footerNumberPage(doc, String((this.numberPage += 1)));
    return doc;
  }

  getList(doc) {
    doc.page = 1;
    doc = this.header(doc);
    let numberPage = 1;
    let line = 0;
    let initLinePage = 52;
    line = initLinePage;

    let positionLeft = 8;
    //let footerLines = 3;

    let maxNumberLinesOfPage = 13;
    let countLinesInsert = 0;
    let dataLength = 11;

    //let printFooterEnd = false;
    let numberOfPages =
      dataLength >= maxNumberLinesOfPage
        ? Math.ceil(Number(dataLength / maxNumberLinesOfPage))
        : 1;

    // Before adding new content
    let y = 200; // Height position of new content
    doc = functUtils.footerNumberPage(doc, String(numberPage));
    let title = ["Viernes 23, sep 2020   SEMANA # 3-20"];
    doc = functUtils.titleDetailsParams(doc, title);

    for (let i = 1; i <= dataLength; i++) {
      doc = this.textoBold(doc, 10, positionLeft, line, `[ ${i} ] `);
      doc = this.textoBold(
        doc,
        11,
        positionLeft + 20,
        line,
        String(" - apodo -").toUpperCase()
      );

      //name employee
      doc = this.textoNormal(
        doc,
        10,
        positionLeft + 10,
        (line += 6),
        "NOMBRE DEL EMPLEADO LARGO PARA DEMO  " + i
      );

      //Amount
      doc = this.textoMonto(
        doc,
        10,
        positionLeft + 50,
        (line += 4),
        `Monto A Cobrar : $1,500.00`
      );

      //line firma
      doc = this.textoNormal(
        doc,
        7,
        positionLeft + 120,
        (line += 1),
        `_________________________________________________`
      );

      //Amount
      doc = this.textoNormal(
        doc,
        7,
        positionLeft + 140,
        (line += 3),
        `Firma, Recibio Conforme`
      );

      line += 3;
      countLinesInsert++;
      //#define new page
      if (
        countLinesInsert == maxNumberLinesOfPage &&
        numberPage != numberOfPages
      ) {
        doc.addPage();
        numberPage++;
        doc = this.header(doc);
        countLinesInsert = 0;
        line = initLinePage;

        //add titleDetaisParams
        let title = ["Nomina de Pago : Viernes 23, sep 2020", "Semana : 44"];
        doc = functUtils.titleDetailsParams(doc, title);

        //add number Of page
        doc = functUtils.footerNumberPage(doc, String(numberPage));
      }
    }

    // doc = functUtils.footerDetailsRight(doc, "Para demo de Algun Texto ", 10);

    return doc;
  }

  //#region "font Report"

  textoBold(doc, size, x, y, value) {
    doc = functUtils.fontCurrierBold(doc, size);
    doc.text(x, y, value);

    return doc;
  }

  textoNormal(doc, size, x, y, value) {
    doc = functUtils.fontHelveticaItalic(doc, size);
    doc.text(x, y, value);
    return doc;
  }

  textoMonto(doc, size, x, y, value) {
    doc = functUtils.fontCurrierBold(doc, size);
    doc.text(x, y, value);
    return doc;
  }
  //#endregion

  getList2(doc) {
    let line = 0;
    let initLinePage = 50;
    line = initLinePage;
    for (let i = 0; i <= 500; i++) {
      doc = functUtils.fontCurrierNormal(doc, 9);
      doc.text(8, line, "Texto -- " + i);
      line += 5;
    }

    let pageHeight = doc.internal.pageSize.height;
    var dim = doc.getTextDimensions("Text");
    console.log(dim);

    return doc;
  }
}

export default ListEmpPayRoll;
