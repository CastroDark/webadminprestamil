import { Injectable } from "@angular/core";
declare var require: any;
var JsBarcode = require("jsbarcode");

import * as jsPDF from "jspdf";

import { ListEmpPayRoll } from "./index";
import { UtilsPrint } from "./index";

const functUtils = new UtilsPrint();

@Injectable({
  providedIn: "root"
})
export class JsPdfService {
  private storage: Storage;

  constructor() {}

  //#region "List Employee to firme payroll"
  printListEmployeeToPayRoll() {
    const calling = new ListEmpPayRoll();

    var doc = new jsPDF("p", "mm", "a4");

    //TITLE REPORT AND DATE
    doc == calling.print(doc);

    var blob = doc.output("blob");
    window.open(URL.createObjectURL(blob));
  }

  //#endregion

  //#region Receipt
  //#endregion
}
