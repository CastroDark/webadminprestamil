import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { HttpHeaders } from "@angular/common/http";

import { Observable, throwError } from "rxjs";
import { retry, catchError } from "rxjs/operators";
import { IUserLogin } from "../../../Interfaces/interfacesPublics";
import * as configuration from "../../../configuration.json";
@Injectable({
  providedIn: "root"
})
// @Injectable({
//     providedIn: 'root',
// })
export class HttpApiService {
  //abstract endpoint;

  url = configuration.conecctionApi; //;

  constructor(protected http: HttpClient) {}

  public async get<G>(request: string): Promise<G | null> {
    let response = null;
    try {
      response = await this.http.get<G>(`${this.url}/${request}`).toPromise();
    } catch (error) {
      response = this.errorHandl;
    }
    return response;
  }

  //#region AUTHORIZED
  // POST
  public LoginUser(
    user: string,
    password: string,
    remenber: boolean
  ): Observable<string> {
    let controller: string = "authorize";
    let method: string = "loginUser";

    var data: IUserLogin = {
      Password: password,
      User: user,
      Remenber: remenber
    };

    return this.http.post<string>(`${this.url}/api/Authorize/loginuser`, data);

    // return this.http
    //     .post<any>(
    //         this.url + `/api/${controller}/${method}`,
    //         JSON.stringify(data),
    //     )
    //     .pipe(
    //         retry(1),
    //         catchError(this.errorHandl),
    //     );
  }

  // POST
  public getProfileUserOnline(): Observable<string> {
    let controller: string = "authorize";
    let method: string = "loginUser";

    return this.http.post<string>(
      `${this.url}/api/Authorize/sendprofileuseronline`,
      {}
    );

    // return this.http
    //     .post<any>(
    //         this.url + `/api/${controller}/${method}`,
    //         JSON.stringify(data),
    //     )
    //     .pipe(
    //         retry(1),
    //         catchError(this.errorHandl),
    //     );
  }

  public getProfileCompany(): Observable<string> {
    return this.http.post<string>(
      `${this.url}/api/Authorize/sendProfileCompany`,
      {}
    );
  }
  //#endregion

  // Error handling
  errorHandl(error) {
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
