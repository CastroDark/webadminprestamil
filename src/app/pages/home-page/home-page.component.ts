import {
    Component,
    OnInit,
    AfterViewInit,
    OnDestroy,
    HostListener,
} from '@angular/core';
import { StorageService } from '../../core/services/storage/storage.service';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';

import { Subject } from 'rxjs';
import * as jsPDF from 'jspdf';

import { AlertManagerService } from '../../core/services/alertSweet/alertManager.service';

@Component({
    selector: 'app-home-page',
    template: `
        <router-outlet></router-outlet>
    `,
    // templateUrl: './home-page.component.html',
    // styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit, OnDestroy {
    nameMOdal: any;
    showModal: boolean = true;
    eventsSubject: Subject<void> = new Subject<void>();

    count = 0;

    constructor(
        private storage: StorageService,
        private router: Router,
        private alert: AlertManagerService,
    ) {}

    ngOnInit() {
        // this.router.events.subscribe(res => {
        //     if (res instanceof NavigationEnd) {
        //         if (res.url == this.router.url) {
        //             console.log(this.router.url);
        //         }
        //         // console.log(this.router.url);
        //         // console.log('NavigationEnd Event' + res);
        //     }
        // });
        //console.log(this.storage.getProfileCompany().idCompany);
        this.showModal = false;
        // setTimeout(() => {
        //     this.showMsg();
        // }, 2000);
        console.log('oninti');
    }
    @HostListener('window:beforeunload')
    onDestroy() {
        console.log('destroid');
    }

    ngOnDestroy() {}

    demo() {
        this.router.navigate(['nav/home/home2'], { replaceUrl: true });
    }

    //test NgMOdalBoostrap

    open(value: any) {
        // const modalRef = this.modalService.open(DemoValidationComponent);
        // let user = {
        //     name: 'Izzat Nadiri',
        //     age: 26,
        // };
        // modalRef.componentInstance.user = user;
        // modalRef.componentInstance.passEntry.subscribe(receivedEntry => {
        //     console.log(receivedEntry);
        // });
        this.nameMOdal = value;
        setTimeout(() => {
            // this.nameMOdal = '';
        }, 300);
        // //console.log(this.nameMOdal);
        this.eventsSubject.next();
    }

    close(value) {
        console.log('close ' + value);
        this.nameMOdal = '';
    }

    print() {
        // Landscape export, 2×4 inches
        var doc = new jsPDF();

        doc.text('Hello world!', 10, 10);
        doc.save('a4.pdf');
    }

    showAlert() {
        // this.alert
        //     .showQuestion({
        //         title: 'Seguro de Eliminar ,,',
        //         text: 'eLIMINAR COSA X',
        //         msgFinish: true,
        //     })
        //     .then(
        //         si => {
        //             console.log('user_si');
        //         },
        //         no => {
        //             console.log('user_no');
        //         },
        //     );

        this.alert.showALertTimer().then(
            yes => {
                this.count++;
            },
            cancel => {
                console.log(cancel);
            },
        );
    }
}
